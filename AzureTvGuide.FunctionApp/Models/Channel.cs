using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AzureTvGuide.FunctionApp.Models
{
    public class Channel
    {
        [JsonProperty("id")] 
        public string Id { get; set; }

        [JsonProperty("name")] 
        public string Name { get; set; }
        
        [JsonProperty("icon",
            NullValueHandling = NullValueHandling.Ignore)]
        public string Icon { get; set; }

        [JsonProperty("programmes")] 
        public List<Programme> Programmes { get; } = new List<Programme>();
    }
}