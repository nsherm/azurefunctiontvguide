using System.Collections.Generic;
using Newtonsoft.Json;

namespace AzureTvGuide.FunctionApp.Models
{
    public class TvListings
    {
        [JsonProperty("channels")] 
        public List<Channel> Channels { get; } = new List<Channel>();
    }
}