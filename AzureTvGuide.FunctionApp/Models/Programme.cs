using System;
using Newtonsoft.Json;

namespace AzureTvGuide.FunctionApp.Models
{
    public class Programme
    {
        [JsonProperty("name")] public string Name { get; set; }
        
        [JsonProperty("start")]
        public DateTime Start { get; set; }
        
        [JsonProperty("end")]
        public DateTime End { get; set; }
        
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}