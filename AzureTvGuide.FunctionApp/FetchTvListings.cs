using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using AzureTvGuide.FunctionApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace AzureTvGuide.FunctionApp
{
    public static class FetchTvListings
    {
        private const string DateFormat = "yyyyMMddHHmmss K";
        
        [FunctionName("FetchTvListings")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]
            HttpRequest req, ILogger log)
        {

            var reader = new XmlTextReader("http://www.xmltv.co.uk/feed/6145");
            var doc = XDocument.Load(reader);

            var tvListings = new TvListings();

            foreach (var xElement in doc.Descendants("channel"))
            {
                ParseChannel(tvListings, xElement);
            }
            
            foreach (var xElement in doc.Descendants("programme"))
            {
                ParsePrograme(tvListings, xElement);
            }


            return new OkObjectResult(tvListings);
        }
        
        private static void ParseChannel(TvListings tvListings, XElement channelElement)
        {
            var icon = channelElement.Descendants("icon").FirstOrDefault();
            
            tvListings.Channels.Add(new Channel
            {
                Name = channelElement.Descendants("display-name").First().Value,
                Id = channelElement.Attribute("id")?.Value,
                Icon = icon != null ? "http://www.xmltv.co.uk" + icon.Attribute("src")?.Value : null
            });
        }

        private static void ParsePrograme(TvListings tvListings, XElement programmeElement)
        {
            var programme = new Programme
            {
                Name = programmeElement.Descendants("title").First().Value,
                Description = programmeElement.Descendants("desc").First().Value,
                Start = DateTime.ParseExact(programmeElement.Attribute("start")?.Value, 
                    DateFormat, CultureInfo.CurrentCulture),
                End = DateTime.ParseExact(programmeElement.Attribute("stop")?.Value, 
                    DateFormat, CultureInfo.CurrentCulture),
            };

            var channel = 
                tvListings.Channels.FirstOrDefault(c => c.Id == programmeElement.Attribute("channel")?.Value);
                        
            channel?.Programmes.Add(programme);
        }
    }
}